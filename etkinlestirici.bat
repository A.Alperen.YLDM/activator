@echo off
:: BatchGotAdmin
::-------------------------------------
REM  --> Check for permissions
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

REM --> If error flag set, we do not have admin.
if '%errorlevel%' NEQ '0' (
    echo Yonetici Haklari Denetleniyor....
    goto UACPrompt
) else ( goto gotAdmin )

:UACPrompt
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params = %*:"="
    echo UAC.ShellExecute "cmd.exe", "/c %~s0 %params%", "", "runas", 1 >> "%temp%\getadmin.vbs"

    "%temp%\getadmin.vbs"
    del "%temp%\getadmin.vbs"
    exit /B

:gotAdmin
    pushd "%CD%"
    CD /D "%~dp0"
::--------------------------------------

powershell -Command "(New-Object Net.WebClient).DownloadFile('https://gitlab.com/A.Alperen.YLDM/activator/-/raw/main/kms_servers.txt?inline=false', '%Temp%/kms_servers.txt')"

set pinged_url=
cd /D %Temp%
for /f %%i in (%temp%\kms_servers.txt) do (
  ping -n 1 %%i > nul
  if %errorlevel% == 0 (
    set pinged_url=%%i
    del %temp%\kms_servers.txt
    goto etkinlestirme
  )
)

:etkinlestirme
echo Windows surumu bulunuyor...
systeminfo | findstr /C:"OS Name" > osversion.txt
set /p osversion= < osversion.txt
set edition=%osversion:~27%

if "%edition%" == "Microsoft Windows 10 Pro N" (
    cls
    echo Aktif Anahtar Etkinlestirme Sunucusu Secildi: %pinged_url%
    echo %edition% Etkinlestirme Baslatiliyor...
    timeout 5
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ipk MH37W-N47XK-V7XM9-C7227-GCQG9
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /skms %pinged_url%
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ato
    timeout 5
    shutdown -r
    ) else if "%edition%" == "Microsoft Windows 10 Pro" (
    cls
    echo Aktif Anahtar Etkinlestirme Sunucusu Secildi: %pinged_url%
    echo %edition% Etkinlestirme Baslatiliyor...
    timeout 5
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ipk W269N-WFGWX-YVC9B-4J6C9-T83GX
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /skms %pinged_url%
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ato
    timeout 5
    shutdown -r
) else if "%edition%" == "Microsoft Windows 11 Pro" (
    cls
    echo Aktif Anahtar Etkinlestirme Sunucusu Secildi: %pinged_url%
    echo %edition% Etkinlestirme Baslatiliyor...
    timeout 5
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ipk W269N-WFGWX-YVC9B-4J6C9-T83GX
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /skms %pinged_url%
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ato
    timeout 5
    shutdown -r
) else if "%edition%" == "Microsoft Windows 10 Home (Single Language)" (
    cls
    echo Aktif Anahtar Etkinlestirme Sunucusu Secildi: %pinged_url%
    echo %edition% Etkinlestirme Baslatiliyor...
    timeout 5
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ipk 7HNRX-D7KGG-3K4RQ-4WPJ4-YTDFH
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /skms %pinged_url%
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ato
    timeout 5
    shutdown -r
) else if "%edition%" == "Microsoft Windows 10 Home (Country Specific)" (
    cls
    echo Aktif Anahtar Etkinlestirme Sunucusu Secildi: %pinged_url%
    echo %edition% Etkinlestirme Baslatiliyor...
    timeout 5
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ipk PVMJN-6DFY6-9CCP6-7BKTT-D3WVR
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /skms %pinged_url%
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ato
    timeout 5
    shutdown -r
) else if "%edition%" == "Microsoft Windows 10 Home N" (
    cls
    echo Aktif Anahtar Etkinlestirme Sunucusu Secildi: %pinged_url%
    echo %edition% Etkinlestirme Baslatiliyor...
    timeout 5
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ipk 3KHY7-WNT83-DGQKR-F7HPR-844BM
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /skms %pinged_url%
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ato
    timeout 5
    shutdown -r
) else if "%edition%" == "Microsoft Windows 10 Home" (
    cls
    echo Aktif Anahtar Etkinlestirme Sunucusu Secildi: %pinged_url%
    echo %edition% Etkinlestirme Baslatiliyor...
    timeout 5
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ipk TX9XD-98N7V-6WMQ6-BX7FG-H8Q99
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /skms %pinged_url%
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ato
    timeout 5
    shutdown -r
) else if "%edition%" == "Microsoft Windows 11 Home" (
    cls
    echo Aktif Anahtar Etkinlestirme Sunucusu Secildi: %pinged_url%
    echo %edition% Etkinlestirme Baslatiliyor...
    timeout 5
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ipk TX9XD-98N7V-6WMQ6-BX7FG-H8Q99
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /skms %pinged_url%
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ato
    timeout 5
    shutdown -r
) else if "%edition%" == "Microsoft Windows 10 Enterprise LTSC Evaluation" (
    cls
    echo Aktif Anahtar Etkinlestirme Sunucusu Secildi: %pinged_url%
    echo %edition% Etkinlestirme Baslatiliyor...
    timeout 5
    powershell -Command "(New-Object Net.WebClient).DownloadFile('https://gitlab.com/A.Alperen.YLDM/activator/-/raw/main/skus.zip', '%Temp%/skus.zip')"
    powershell.exe /c "Expand-Archive -Force .\skus.zip"
    powershell.exe /c "Copy-Item -Path .\skus -Destination %windir%\system32\spp\tokens -Recurse"
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /rilc
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /upk
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ckms
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /cpky
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ipk M7XTQ-FN8P6-TTKYV-9D4CC-J462D
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /skms %pinged_url%
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ato
    del %Temp%\skus.zip
    rmdir /S /Q %Temp%\skus
    timeout 5
    shutdown -r
) else if "%edition%" == "Microsoft Windows 10 Enterprise LTSC" (
    cls
    echo Aktif Anahtar Etkinlestirme Sunucusu Secildi: %pinged_url%
    echo %edition% Etkinlestirme Baslatiliyor...
    timeout 5
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ipk M7XTQ-FN8P6-TTKYV-9D4CC-J462D
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /skms %pinged_url%
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ato
    timeout 5
    shutdown -r
) else if "%edition%" == "Microsoft Windows 10 Enterprise N" (
    cls
    echo Aktif Anahtar Etkinlestirme Sunucusu Secildi: %pinged_url%
    echo %edition% Etkinlestirme Baslatiliyor...
    timeout 5
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ipk DPH2V-TTNVB-4X9Q3-TJR4H-KHJW4
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /skms %pinged_url%
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ato
    timeout 5
    shutdown -r
) else if "%edition%" == "Microsoft Windows 10 Enterprise" (
    cls
    echo Aktif Anahtar Etkinlestirme Sunucusu Secildi: %pinged_url%
    echo %edition% Etkinlestirme Baslatiliyor...
    timeout 5
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ipk NPPR9-FWDCX-D2C8J-H872K-2YT43
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /skms %pinged_url%
    %windir%\system32\cscript.exe %windir%\system32\slmgr.vbs /ato
    timeout 5
    shutdown -r
) else (
    echo The code is not working for this edition.
    pause
)